const Koa = require('koa');
const Router = require('koa-router');
const cors = require('@koa/cors');
const bodyparser = require('koa-bodyparser');

// Initialize vars
const app = new Koa();
app.use(cors());
app.use(bodyparser());
const port = 3000;

// Routers
const router = new Router();
const botato = require('./routers/botato');
const doltato = require('./routers/doltato');
const notato = require('./routers/notato');
const nutrato = require('./routers/nutrato');

// Routes
router.get('/', (ctx,next) => {
    ctx.body = "hello world";
});

// Middleware
app
    .use(router.routes())
    .use(router.allowedMethods())
    .use(botato.routes())
    .use(botato.allowedMethods())
    .use(doltato.routes())
    .use(doltato.allowedMethods())
    .use(notato.routes())
    .use(notato.allowedMethods())
    .use(nutrato.routes())
    .use(nutrato.allowedMethods());


app.listen(port, () => {
    console.log("App listening on: " + port +".");
});