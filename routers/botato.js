const Router = require('koa-router');
const botdb = require('../db/botatoDB');

const bot = botdb.bot;

// RouterConfig
var br = new Router({
    prefix: '/api/botato'
});

// BOT getall
br.get('/bots',async (ctx,next)=>{
    ctx.body = await findBot();
});
async function findBot(){
    return await bot.find();
}
// BOT create
br.post('/bots',async (ctx,next) => {
    ctx.body = await addBot(ctx);
});
async function addBot(ctx){
    try{
        return await new bot(ctx.request.body).save();;
    }catch(err){
        ctx.throw(422);
    }
}
// BOT getOne
br.get('/bots/:id',async (ctx,nex) => {
    ctx.body = await findBotById(ctx);
});
async function findBotById(ctx){
    try{
        const result = await bot.findOne({
            botId: ctx.params.id
        });
        if(!result){
            ctx.throw(404);
        }
        return result;
    }catch(err){
        if (err.name === 'CastError' || err.name === 'NotFoundError') {
            ctx.throw(404);
        }
        ctx.throw(500);
    }
}
// BOT update
br.put('/bots/:id',async (ctx,next) => {
    ctx.body = await updateBot(ctx);
});
async function updateBot(ctx){
    try{
        const result = await bot.findOneAndUpdate(
            {botId: ctx.params.id},
            ctx.request.body,
            {upsert: true, new:true}
        );
        if(!result){
            ctx.throw(404);
        }
        return result;
    }catch(err){
        if(err.name === 'CastError' || err.name === 'NotFoundError'){
            ctx.throw(404);
        }
        ctx.throw(500);
    }
}
// BOT delete
br.del('/bots/:id',async (ctx,next) => {
    ctx.body = await deleteBot(ctx);
});
async function deleteBot(ctx){
    try{
        const result = await bot.findOneAndRemove({botId:ctx.params.id});
        if(!result){
            ctx.throw(404);
        }
        return result;
    }catch(err){
        if(err.name === 'CastError' || err.name === 'NotFoundError'){
            ctx.throw(404);
        }
        ctx.throw(500);
    }
}

module.exports = br;