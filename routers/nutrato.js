const Router = require('koa-router');
const mongoose = require('mongoose');

const nutdbc = mongoose.createConnection("mongodb://127.0.0.1:27017/nutrato");

const nutrato = new Router({
    prefix: '/api/nutrato'
});

nutrato.get('/products', (ctx,next) => {
    ctx.body = "Nutrato Products Getall()";
});

module.exports = nutrato;